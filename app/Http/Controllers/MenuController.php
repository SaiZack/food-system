<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Menu;

class MenuController extends Controller
{
    // /**
    //  * Display a listing of the resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    public function index()
    {
        $menuData = Menu::all();
        return view('menu.index', compact('menuData'));
    }

    // /**
    //  * Show the form for creating a new resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    public function create()
    {
        return view('menu.create');
    }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    public function store(Request $request)
    {
        if(!$request["name"]){
            return redirect()->route('menu.create')->with('error', 'Please Enter Menu Name');    
        }
        else if(!$request->file('cover')){
            return redirect()->route('menu.create')->with('error', 'Please Choose Cover Image');    
        }
        else{
            $data= new Menu();
            if($request->file('cover')){
                $file= $request["cover"];
                $path = "storage/images/menu/";
                $filename= date('YmdHi').$file->getClientOriginalName();
                $file-> storeAs('public/images/menu/', $filename);
                $data['cover']= $path.$filename;
            }
            $data['name'] = $request->name;
            $data->save();
            return redirect()->route('menu.index')->with('success', 'Add Menu Successfully.');
        }
    }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function show($id)
    {
        
    }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function edit(Menu $menu)
    {
    
        return view('menu.edit',[
            'menu'=>$menu
        ]);
    }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function update(Request $request, Menu $menu)
    {
        if($request->file('cover')){
            $file= $request->file('cover');
            $path = "storage/images/menu/";
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file-> storeAs('public/images/menu/', $filename);

            if(file_exists($menu->cover)){
                unlink($menu->cover);
            }
            $menu->cover = $path.$filename;
        }
        $menu->name = $request->name;
        $menu->save();
        return redirect()->route('menu.index')->with('success','Menu updated successfully');
    }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function destroy(Menu $menu)
    {
        // delete related   
        $menu->item()->delete();

        $menu->delete();
        return redirect()->route('menu.index')->with('success', 'Delete Menu successfully.');
    }
}
