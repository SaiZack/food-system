<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Item;
use App\Models\Menu;
use App\Models\Shop;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $itemData= Item::all();
        return view('item.index', compact('itemData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menus = Menu::all();
        $shops = Shop::all();
        return view('item.create')->with([
            'menus' => $menus,
            'shops' => $shops
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->name){
            return redirect()->route('item.create')->with('error', 'Please Enter Item Name');
        }
        else if(!$request->price){
            return redirect()->route('item.create')->with('error', 'Please Enter Item Price');
        }
        else if(!$request->menu_id){
            return redirect()->route('item.create')->with('error', 'Please Select Menu');
        }
        else if(!$request->shop_id){
            return redirect()->route('item.create')->with('error', 'Please Select Shop');
        }
        else if(!$request->file('cover')){
            return redirect()->route('item.create')->with('error', 'Please Choose Cover Image');
        }
        else if(!$request->file('images')){
            return redirect()->route('item.create')->with('error', 'Please Choose Images');
        }
        else{
            $data= new Item();

            if($request->file('cover')){
                $file= $request->file('cover');
                $path = "storage/images/item/";
                $filename= date('YmdHi').$file->getClientOriginalName();
                $file-> storeAs('public/images/item/', $filename);
                $data['cover'] = $path.$filename;
            }
            if($request->file('images')){
                $files= $request->file('images');
                foreach($files as $file){
                    $filename= date('YmdHi').$file->getClientOriginalName();
                    $path = "storage/images/item/";
                    $file-> storeAs('public/images/item/', $filename);
                    $imgs[] = $path.$filename;
                }
                $imgstr = implode(", ", $imgs);
                $data['image'] = $imgstr;
            }
            $data['name'] = $request->name;
            $data['price'] = $request->price;
            $data['menu_id'] = $request->menu_id;
            $data['shop_id'] = $request->shop_id;
            $data->save();
            return redirect()->route('item.index')->with('success', 'Add Item successfully.');
        }
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        $menus = Menu::all();
        $shops = Shop::all();
        return view('item.edit')->with([
            'item'=>$item,
            'menus' => $menus,
            'shops' => $shops
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        if($request->file('cover')){
            $file= $request->file('cover');
            $path = "storage/images/item/";
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file-> storeAs('public/images/item/', $filename);

            if(file_exists($item->cover)){
                unlink($item->cover);
            }

            $item->cover = $path.$filename;
        }
        if($request->file('images')){
            $files= $request->file('images');
            foreach($files as $file){
                $filename= date('YmdHi').$file->getClientOriginalName();
                $path = "storage/images/item/";
                $file-> storeAs('public/images/item/', $filename);
                $imgs[] = $path.$filename;
            }
            // dd($item->image);
            $oldimgs = explode(", ", $item->image);
            foreach ($oldimgs as $oldimg){
                if(file_exists($oldimg)){
                    unlink($oldimg);
                }
            }

            $imgstr = implode(", ", $imgs);
            $item->image = $imgstr;
        }
        $item->name = $request->name;
        $item->price = $request->price;
        $item->menu_id = $request->menu_id;
        $item->shop_id = $request->shop_id;
        $item->save();
        return redirect()->route('item.index')->with('success','Item updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $item->delete();
        return redirect()->route('item.index')->with('success', 'Delete Item successfully.');
    }
}
