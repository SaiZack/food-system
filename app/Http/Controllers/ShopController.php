<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Shop;

class ShopController extends Controller
{
    // /**
    //  * Display a listing of the resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    public function index()
    {
        $shopData= Shop::all();
        return view('shop.index', compact('shopData'));
    }

    // /**
    //  * Show the form for creating a new resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    public function create()
    {
        return view('shop.create');
    }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    public function store(Request $request)
    {
        if(!$request["name"]){
            return redirect()->route('shop.create')->with('error', 'Please Enter Shop Name');    
        }
        else if(!$request->file('logo')){
            return redirect()->route('shop.create')->with('error', 'Please Choose Shop Logo');    
        }
        else{
            $data= new Shop();

            if($request->file('logo')){
                $file= $request->file('logo');
                $path = "storage/images/shop/";
                $filename= date('YmdHi').$file->getClientOriginalName();
                $file-> storeAs('public/images/shop/', $filename);
                $data['logo']= $path.$filename;
            }
            $data['name'] = $request->name;
            $data['address'] = $request->address;
            $data->save();
            return redirect()->route('shop.index')->with('success', 'Add Shop successfully.');
        }
    }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function show($id)
    {
        //
    }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function edit(Shop $shop)
    {
        return view('shop.edit',[
            'shop'=>$shop
        ]);
    }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function update(Request $request, Shop $shop)
    {
        if($request->file('logo')){
            $file= $request->file('logo');
            $path = "storage/images/shop/";
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file-> storeAs('public/images/shop/', $filename);

            if(file_exists($shop->logo)){
                unlink($shop->logo);
            }

            $shop->logo = $path.$filename;
        }
        $shop->name = $request->name;
        $shop->address = $request->address;
        $shop->save();
        return redirect()->route('shop.index')->with('success','Shop updated successfully');
    }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function destroy(Shop $shop)
    {
        $shop->item()->delete();
        $shop->delete();
        return redirect()->route('shop.index')->with('success', 'Delete Shop successfully.');
    }
}
