@extends('layout')

@section('title', 'Add Menu')

@section('style', '1')

@section("content")
<div class="container w-50"><br><br><br>

@include('common.alert')

  <h2><i class="fa-solid fa-plus"></i> Add New Menu</h2>
  <hr>
  <form method="post" action="{{ route('menu.store') }}" enctype="multipart/form-data">
    @csrf
    <div>
      <label><h4>Enter Name *</h4></label>
      <input type="text" class="form-control" name="name">
    </div>
    <br>
    <div>
      <label><h4>Add Cover *</h4></label>
      <input type="file" class="form-control" name="cover">
    </div>
    <br><br>
    <div class="mb-5">
      <button type="submit" class="btn btn-success px-5">Add Menu</button>
    </div>
  </form>
</div>
@endsection