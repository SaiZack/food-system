@extends('layout')

@section('title', 'Edit Menu')

@section('style', '1')

@section("content")
<div class="container w-50"><br><br><br>
  <h2><i class="fa-solid fa-pen-to-square"></i> Edit Menu</h2>
  <hr>
  <form method="post" action="{{route('menu.update', ['menu' => $menu->id])}}" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div>
      <label><h4>Name</h4></label>
      <input type="text" class="form-control" name="name" value="{{$menu->name}}">
    </div>
    <br>
    <div>
      <label><h4>Old Cover</h4></label><br>
      <img src="{{ asset($menu->cover) }}" alt="old image"style="width: 200px;" class="image shadow rounded" onclick="showImage(this.src)">
    </div>
    <br>
    <div>
      <label><h4>New Cover</h4></label>
      <input type="file" class="form-control" name="cover">
    </div>
    <br><br>
    <div class="mb-5">
      <button type="submit" class="btn btn-success px-5">Edit Menu</button>
    </div>
  </form>
</div>
@endsection