@extends('layout')

@section('title', 'Menu List')

@section('style', '1')

@section("content")
<div class="container"><br><br><br>
@include('common.alert')
<h3 class="float-start">MENUs</h3>
    <a href="{{route('menu.create')}}" class="btn btn-success px-5 mx-3 float-end"><i class="fa-regular fa-plus"></i> ADD NEW SHOP</a>
    <br><br><hr>
    <table class="table table-striped m-3 p-5">
      <thead>
        <tr>
          <th scope="col">Id</th>
          <th scope="col">Name</th>
          <th scope="col">Cover</th>
        </tr>
      </thead>
      <tbody>
        @foreach($menuData as $data)
        <tr>
          <td>{{$data->id}}</td>
          <td>{{$data->name}}</td>
          <td>
            <img src="{{ asset($data->cover) }}" class="rounded shadow" style="width: 200px;">
          </td>
          <td><a class="btn btn-outline-primary" href="{{ route('menu.edit',$data->id) }}"><i class="fa-solid fa-pen-to-square"> </i> Edit</a></td>
          <td>
              <form action="{{ route('menu.destroy',$data->id) }}" method="POST">
                  @csrf
                  @method('DELETE')

                  <button type="submit" class="btn btn-outline-danger" onclick="return confirm('Are you sure to DELETE?')"><i class="fa-solid fa-trash-can"> </i> Delete</button>
              </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection