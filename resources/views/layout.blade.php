<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Food System - @yield('title')</title>
        <script src="https://kit.fontawesome.com/59306e82f2.js" crossorigin="anonymous"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            .image, tr{
                transition: 0.3s ease;
                cursor:pointer;
            }
            .image:hover, tbody>tr:hover{
                scale: 1.07;
            }
            .nav-link::after{
                content: '';
                position: absolute;
                bottom: -8px;
                left:0;
                width: 0%;
                height: 5px;
                background-color: #fff;
                border-radius: 5px 5px 0 0;
                transition: 0.2s ease;
            }
            .nav-link:hover::after{
                width: 100%;
            }
            .nav-link:nth-child( @yield('style') ){
            color: rgba(var(--bs-primary-rgb),1);
            }
            .nav-link:nth-child( @yield('style') )::after{
                width:100%;
                height: 130%;
                z-index: -1;
                animation: active 0.3s linear 1;
            }
            @keyframes active {
                0%{
                    height: 5px;
                }
                100%{
                    height: 130%;
                }
            }
            .image-hide{
                display: none !important;
            }
        </style>
    </head>
    <body class="antialiased position-relative">
        <div class="position-fixed w-100 vh-100 d-flex justify-content-center align-items-center image-hide" style="background: #0005; z-index:10" id="imgDisplay">
            <img src="source" alt='image' style='max-height: 75vh' class='rounded shadow image' id="full_img"/>
        </div>
            <nav class="navbar navbar-expand-lg bg-primary text-light position-fixed w-100" style="z-index:5;">
                <a href="{{route('menu.index')}}" class="nav-link px-3 py-1 mx-3 position-relative"><i class="fa-solid fa-house"></i> Menu</a>
                <a href="{{route('shop.index')}}" class="nav-link px-3 py-1 mx-3 position-relative"><i class="fa-solid fa-cart-shopping"></i> Shop</a>
                <a href="{{route('item.index')}}" class="nav-link px-3 py-1 mx-3 position-relative"><i class="fa-solid fa-tag"></i> Item</a>
            </nav>
            <!-- Content -->
            @yield('content')
               
    </body>
    <script>
            var display = document.getElementById('imgDisplay');
            var full_img = document.getElementById('full_img');
            function showImage(source){
                display.classList.remove('image-hide');
                // console.log(source);
                full_img.src = source;
            }
            display.addEventListener('click', function(){
                display.classList.add('image-hide');
            })
        </script>     
</html>
