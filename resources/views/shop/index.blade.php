@extends('layout')

@section('title', 'Shop List')

@section('style', '2')

@section("content")
<div class="container"><br><br><br>
@include('common.alert')
    <h3 class="float-start">SHOPs</h3>
    <a href="{{route('shop.create')}}" class="btn btn-success px-5 mx-3 float-end"><i class="fa-regular fa-plus"></i> ADD NEW SHOP</a>
    <br><br><hr>
       <div class="row justify-content-center align-items-start">
          @foreach($shopData as $data)
            <div class="card col-4 m-4 position-relative py-2 image" style="width: 300px;">
              <div class="position-absolute bg-danger rounded text-light px-3 py-1">ID: {{$data->id}}</div>
              <img src="{{ asset($data->logo) }}" class="card-img-top" alt="logo">
              <div class="card-body">
                <h5 class="card-title">{{$data->name}}</h5>
                <p class="card-text">Address: {{$data->address}}</p>
                <div class="d-flex justify-content-around">
                  <a href="{{ route('shop.edit',$data->id) }}" class="btn btn-outline-primary"><i class="fa-solid fa-pen-to-square"></i> Edit</a>
                  <form action="{{ route('shop.destroy', $data->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-outline-danger"  onclick="return confirm('Are you sure to DELETE?')"><i class="fa-solid fa-trash-can"></i> Delete</button>
                    </form>
                </div>
              </div>
            </div>
            @endforeach
       </div>
  </div>
@endsection