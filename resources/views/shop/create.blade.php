@extends('layout')

@section('title', 'Add Shop')

@section('style', '2')

@section("content")
<div class="container w-50"><br><br><br>

@include('common.alert')

  <h2><i class="fa-solid fa-plus"></i> Add New Shop</h2>
  <hr>
  <form method="post" action="{{ route('shop.store') }}" enctype="multipart/form-data">
    @csrf
    <div>
      <label><h4>Enter Name *</h4></label>
      <input type="text" class="form-control" name="name">
    </div><br>
    <div>
      <label><h4>Enter Address</h4></label>
      <input type="text" class="form-control" name="address">
    </div><br>
    <div>
      <label><h4>Add Logo *</h4></label>
      <input type="file" class="form-control" name="logo">
    </div>
<br>
    <div class="mb-5">
      <button type="submit" class="btn btn-success px-5">Add Shop</button>
    </div>
  </form>
</div>
@endsection