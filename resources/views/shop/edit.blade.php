@extends('layout')

@section('title', 'Edit Shop')

@section('style', '2')

@section("content")
<div class="container w-50"><br><br><br>
  <h2><i class="fa-solid fa-pen-to-square"></i> Edit Shop</h2>
  <hr>
  <form method="post" action="{{route('shop.update', ['shop' => $shop->id])}}" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div>
      <label><h4>Name</h4></label>
      <input type="text" class="form-control" name="name" value="{{$shop->name}}">
    </div><br>
    <div>
      <label><h4>Address</h4></label>
      <input type="text" class="form-control" name="address" value="{{$shop->address}}">
    </div><br>
    <div>
      <label><h4>Old Logo</h4></label><br>
      <img src="{{ asset($shop->logo) }}" alt="old image" style="width:200px" class="rounded shadow image" onclick="showImage(this.src)">
    </div><br>
    <div>
      <label><h4>New Logo</h4></label>
      <input type="file" class="form-control" name="logo">
    </div>
<br>
    <div class="mb-5">
      <button type="submit" class="btn btn-success px-5">Edit Shop</button>
    </div>
  </form>
</div>
@endsection

