@extends('layout')

@section('title', 'Shop List')

@section('style', '2')

@section("content")
<div class="container"><br><br><br>
@include('common.alert')
    <h3>View all Shop</h3><hr>
    <a href="{{route('shop.create')}}" class="btn btn-success px-5 mx-3 float-end"><i class="fa-regular fa-plus"></i> ADD NEW SHOP</a>
    <table class="table table-striped m-3 p-5">
      <thead>
        <tr>
          <th scope="col">Id</th>
          <th scope="col">Name</th>
          <th scope="col">Address</th>
          <th scope="col">Logo</th>
        </tr>
      </thead>
      <tbody>
        @foreach($shopData as $data)
        <tr>
          <td>{{$data->id}}</td>
          <td>{{$data->name}}</td>
          <td>{{$data->address}}</td>
          <td>
            <img src="{{ asset($data->logo) }}" style="width: 200px;" class="shadow rounded">
          </td>
          <td><a href="{{ route('shop.edit',$data->id) }}" class="btn btn-outline-primary"><i class="fa-solid fa-pen-to-square"></i></a></td>
          <td>
              <form action="{{ route('shop.destroy', $data->id) }}" method="POST">
                  @csrf
                  @method('DELETE')

                  <button type="submit" class="btn btn-outline-danger"  onclick="return confirm('Are you sure to DELETE?')"><i class="fa-solid fa-trash-can"></i></button>
              </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection