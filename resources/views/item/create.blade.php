@extends('layout')

@section('title', 'Add Item')

@section('style', '3')

@section("content")
<div class="container w-50"><br><br><br>

@include('common.alert')

  <h2><i class="fa-solid fa-plus"></i> Add New Item</h2>
  <hr>
  <form method="post" action="{{ route('item.store') }}" enctype="multipart/form-data">
    @csrf
    <div>
      <label><h4>Enter Name *</h4></label>
      <input type="text" class="form-control" name="name">
    </div>
    <br>
    <div>
      <label><h4>Add Cover *</h4></label>
      <input type="file" class="form-control" name="cover">
    </div><br>
    <div>
      <label><h4>Add Image *</h4></label>
      <input type="file" class="form-control" name="images[]" multiple>
    </div><br>
    <div>
    </div>
    <div>
      <label><h4>Price *</h4></label>
      <input type="number" class="form-control" name="price">
    </div><br>
    <div>
      <label><h4>Menu *</h4></label>
      <select name="menu_id" class="form-control">
        <option value=" " disabled selected>--Select Menu--</option>
      @foreach($menus as $menu)
        <option value="{{$menu->id}}" >{{$menu->name ?? ''}}</option>
        @endforeach
      </select> 
    </div><br>
    <div>
      <label><h4>Shop *</h4></label>
      <select name="shop_id" class="form-control">
        <option value=" " disabled selected>--Select Shop--</option>
      @foreach($shops as $shop)
        <option value="{{$shop->id}}" >{{$shop->name ?? ''}}</option>
        @endforeach
      </select> 
    </div><br>
    <br>
    <div class="mb-5">
      <button type="submit" class="btn btn-success px-5">Add Item</button>
    </div>
  </form>
</div>
@endsection