@extends('layout')

@section('title', 'Item List')

@section('style', '3')

@section("content")
<div class="container"><br><br><br>
@include('common.alert')
<h3 class="float-start">Item List</h3>
    <a href="{{route('item.create')}}" class="btn btn-success px-5 mx-3 float-end"><i class="fa-regular fa-plus"></i> ADD NEW SHOP</a>
    <br><br><hr>
    <table class="table table-striped m-3 p-5">
      <thead>
        <tr>
          <th scope="col">Id</th>
          <th scope="col">Name</th>
          <th scope="col">Cover</th>
          <th scope="col">Image</th>
          <th scope="col">Price</th>
          <th scope="col">Menu</th>
          <th scope="col">Shop</th>
        </tr>
      </thead>
        @foreach($itemData as $data)
        <tr>
          <td>{{$data->id}}</td>
          <td>{{$data->name}}</td>
          <td>
	          <img src="{{ asset($data->cover) }}"style="width: 200px;" class="rounded shadow">
	        </td>
          
          <td style="width: 300px">
          @foreach(explode(', ', $data->image) as $path)
            <img src="{{ asset($path) }}" style="width: 100px;" class="rounded shadow">
          @endforeach
          </td>
          
          <td>{{$data->price}}</td>
          <td>{{$data->menu->name ?? 'unknown'}}</td>
          <td>{{$data->shop->name ?? 'unknown'}}</td>
          <td><a href="{{ route('item.edit',$data->id) }}" class="btn btn-outline-primary"><i class="fa-solid fa-pen-to-square"></i> Edit</a></td>
          <td>
              <form action="{{ route('item.destroy', $data->id) }}" method="POST">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="btn btn-outline-danger"  onclick="return confirm('Are you sure to DELETE?')"><i class="fa-solid fa-trash-can"></i> Delete</button>
              </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection