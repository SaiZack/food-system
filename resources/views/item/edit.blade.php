@extends('layout')

@section('title', 'Edit Item')

@section('style', '3')

@section("content")
<div class="container w-50"><br><br><br>
  <h2><i class="fa-solid fa-pen-to-square"></i> Edit Item</h2>
  <hr>
  <form method="post" action="{{route('item.update', ['item' => $item->id])}}" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div>
      <label><h4>Name</h4></label>
      <input type="text" class="form-control" name="name" value="{{$item->name}}">
    </div>
    <br>
    <div>
      <label><h4>Old Cover</h4></label><br>
      <img src="{{ asset($item->cover) }}" alt="old image" style="width:200px" class="rounded shadow image" onclick="showImage(this.src)">
    </div><br>
    <div>
      <label><h4>New Cover</h4></label>
      <input type="file" class="form-control" name="cover">
    </div><br>
    <div>
      <label><h4>Old Images</h4></label><br>
      @foreach(explode(', ', $item->image) as $path)
            <img src="{{ asset($path) }}" style="width: 100px;" class="rounded shadow image" onclick="showImage(this.src)">
          @endforeach
    </div><br>
    <div>
      <label><h4>New Images</h4></label>
      <input type="file" class="form-control" name="images[]" multiple>
    </div><br>
    <div>
    </div>
    <div>
      <label><h4>Price</h4></label>
      <input type="text" class="form-control" name="price" value="{{$item->price}}">
    </div><br>
    <div>
      <label><h4>Menu</h4></label>
      <select name="menu_id" class="form-control">
      @foreach($menus as $menu)
        <option value="{{$menu->id}}"  @if($menu->id == $item->menu_id) selected="selected" @endif > {{$menu->name ?? ''}}</option>
        @endforeach
      </select> 
    </div><br>
    <div>
      <label><h4>Shop</h4></label>
      <select name="shop_id" class="form-control">
      @foreach($shops as $shop)
        <option value="{{$shop->id}}" @if($shop->id == $item->shop_id) selected="selected" @endif > {{$shop->name ?? ''}}</option>
        @endforeach
      </select> 
    </div><br>
    <div class="mb-5">
      <button type="submit" class="btn btn-success px-5">Edit Item</button>
    </div>
  </form>
</div>
@endsection

