{{-- Message --}}
@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible my-3" role="alert">
        <strong>Success !</strong> {{ session('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif

@if (Session::has('error'))
    <div class="alert alert-danger alert-dismissible my-3" role="alert">
        <strong>Error !</strong> {{ session('error') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif